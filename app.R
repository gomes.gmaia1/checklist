library(shiny)
library(shinydashboard)
library(DT)
library(shinyjs)
library(sodium)
library(shinyWidgets)

# Main login screen
loginpage <- div(id = "loginpage", style = "width: 500px; max-width: 100%; margin: 0 auto; padding: 20px;",
                 wellPanel(
                   tags$h2("LOG IN", class = "text-center", style = "padding-top: 0;color:#333; font-weight:600;"),
                   textInput("userName", placeholder="Username", label = tagList(icon("user"), "Username")),
                   passwordInput("passwd", placeholder="Password", label = tagList(icon("unlock-alt"), "Password")),
                   br(),
                   div(
                     style = "text-align: center;",
                     actionButton("login", "SIGN IN", style = "color: white; background-color:#3c8dbc;
                                 padding: 10px 15px; width: 150px; cursor: pointer;
                                 font-size: 18px; font-weight: 600;"),
                     shinyjs::hidden(
                       div(id = "nomatch",
                           tags$p("Oops! Incorrect username or password!",
                                  style = "color: red; font-weight: 600; 
                                            padding-top: 5px;font-size:16px;", 
                                  class = "text-center"))),
                     br(),
                     br(),
                     br(),
                     tags$code("Para solicitar login: gomes.gmaia1@gmail.com")
                   )))

credentials = data.frame(
  username_id = c("motipai", "gui", "convidado"),
  passod   = sapply(c("1554236", "1554236", "0000"),password_store),
  permission  = c("advanced", "advanced", "basic"), 
  stringsAsFactors = F)

header <- dashboardHeader( title = "CHECKLIST", uiOutput("logoutbtn"))

sidebar <- dashboardSidebar(uiOutput("sidebarpanel") )
body <- dashboardBody(shinyjs::useShinyjs(), uiOutput("body") )

ui<-dashboardPage(header, sidebar, body, skin = "blue")


server <- function(input, output, session) {
  
  login = FALSE
  USER <- reactiveValues(login = login)
  
  observe({ 
    if (USER$login == FALSE) {
      if (!is.null(input$login)) {
        if (input$login > 0) {
          Username <- isolate(input$userName)
          Password <- isolate(input$passwd)
          if(length(which(credentials$username_id==Username))==1) { 
            pasmatch  <- credentials["passod"][which(credentials$username_id==Username),]
            pasverify <- password_verify(pasmatch, Password)
            if(pasverify) {
              USER$login <- TRUE
            } else {
              shinyjs::toggle(id = "nomatch", anim = TRUE, time = 1, animType = "fade")
              shinyjs::delay(3000, shinyjs::toggle(id = "nomatch", anim = TRUE, time = 1, animType = "fade"))
            }
          } else {
            shinyjs::toggle(id = "nomatch", anim = TRUE, time = 1, animType = "fade")
            shinyjs::delay(3000, shinyjs::toggle(id = "nomatch", anim = TRUE, time = 1, animType = "fade"))
          }
        } 
      }
    }    
  })
  
  output$logoutbtn <- renderUI({
    req(USER$login)
    tags$li(a(icon("fa fa-sign-out"), "Logout", 
              href="javascript:window.location.reload(true)"),
            class = "dropdown", 
            style = "background-color: #eee !important; border: 0;
                    font-weight: bold; margin:5px; padding: 10px;")
  })
  
  output$sidebarpanel <- renderUI({
    if (USER$login == TRUE ){ 
      sidebarMenu(
        menuItem("REAL STATE", tabName = "real_state", icon = icon("building")))
    }
  })
  
  
  
  output$body <- renderUI({
    if (USER$login == TRUE ) {

      tabItem(tabName ="REAL STATE", class = "active",
              fluidRow(div(id = "TimingBox",
                           tabBox(id = "Timing",
                                  tabPanel("31/07/2020", 
                                           dataTableOutput("text")
                                           
                                           , width = 12),
                                  tabPanel("30/07/2020", dataTableOutput("result")
                                           
                                           , width = 12),
                                  title = p("",
                                            div(id = "mybutton", 
                                                dropdownButton(
                                                  "Filtros", 
                                                  selectInput("codeInput",label ="Setor",
                                                              choices = (unique(df$tipo_fundo)), multiple = T, selected = "Shoppings"),
                                                  selectInput("varInput",label ="Indicadores",
                                                              choices = colnames(df[,-c(1,2,8,9)]), multiple = T, selected = colnames(df[,-c(1,2,8,9)])),
                                                  circle = TRUE,
                                                  size = 'xs',
                                                  right = TRUE,
                                                  icon = icon("gear"),
                                                  width = '100px'
                                                ))), 
                                  width = 12,
                                  selected = "31/07/2020"
                           )
              )))
      
    }
    else {
      loginpage
    }
  })
  
  
 
  df <- read.csv('fundos3107.csv')
  df$tipo_fundo <- as.character(df$tipo_fundo)
  Encoding(df$tipo_fundo) <- "UTF-8"
  row.names(df) <- paste0(df$X.1, " (",df$cotacao, ") (", df$tipo_fundo ,")")
  df <- df[,-c(10:19)]
  
  
  
  filt <- selectInput("codeInput",label ="Setor",
                      choices = (unique(df$tipo_fundo)), multiple = T, selected = "Shoppings")
  
  filt2 <- selectInput("varInput",label ="Indicadores",
                       choices = colnames(df[,-c(1,2,8,9)]), multiple = T, selected = colnames(df[,-c(1,2,8,9)]))
  
  output$codePanel <- renderUI({ filt
    
  })
  
  output$varPanel <- renderUI({ filt2
    
  })
  
  dat <- reactive({
    req(input$codeInput, input$varInput, input$Timing)
    
    if(input$Timing != "31/07/2020"){
    df <- read.csv('fundos.csv')
    df$tipo_fundo <- as.character(df$tipo_fundo)
    Encoding(df$tipo_fundo) <- "UTF-8"
    row.names(df) <- paste0(df$X, " (",df$cotacao, ") (", df$tipo_fundo ,")")
    
    ab <- subset(df, tipo_fundo %in% input$codeInput) 
    ab <- ab[,-c(1,2,8,9)]
    ab <- ab[, names(ab) %in% input$varInput] 

    
    }else{
      ab <- subset(df, tipo_fundo %in% input$codeInput) 
      ab <- ab[,-c(1,2,8,9)]
      ab <- ab[, names(ab) %in% input$varInput] 

    }
    
    return(ab)
  })
  
  
  string <- reactive({
    
    vals <- list()
    
    for(g in 1:ncol(dat())) vals$rm[g] = mean((dat()[,g]))
    for(g in 1:ncol(dat())) vals$rr[g] = abs(max(dat()[,g])-min(dat()[,g]))
    for(g in 1:ncol(dat())) vals$r1[g] = max(dat()[,g]) - mean((dat()[,g]))
    for(g in 1:ncol(dat())) vals$r0[g] = mean((dat()[,g])) - min(dat()[,g])

    
    paste0("function(row, data) {",
           paste(lapply(1:ncol(dat()),function(i){
             paste0("var value2= data[",i,"];
                                       if(value2 < ",vals$rm[i],"){
                                       $(this.api().cell(row,",i,").node()).css({'background' : 'linear-gradient(90deg, transparent ' + (1 - (",vals$rm[i]," - value2)/",vals$rr[i],")* 100 + '%, red ' + (1 - (",vals$rm[i]," - value2)/",vals$rr[i],") * 100 + '%)','background-size':'98% 88%','background-repeat':'no-repeat','background-position':'center'});
                                       }else{
                                       $(this.api().cell(row,",i,").node()).css({'background'  : 'linear-gradient(90deg, transparent ' +  (1 - (",-vals$rm[i]," + value2)/",vals$rr[i],") * 100 + '%, lightgreen ' +   (1 - (",-vals$rm[i]," + value2)/",vals$rr[i],")* 100 + '%)','background-size':'98% 88%','background-repeat':'no-repeat','background-position':'center'});
                                       } ") 
           }),collapse="\n "),
           "} ")
    
  })
  
  

    
    output$text <- renderDT({
      a = cat(string())
      return(dat())},  
      options = list(
        rowCallback=JS(string()) 
      )) 

    output$result <- renderDT({
      a = cat(string())
      return(dat())},  
      options = list(
        rowCallback=JS(string()) 
      )) 
  

}

shinyApp(ui = ui, server = server)
